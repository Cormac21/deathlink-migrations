USE DEATHLINK_2;

INSERT INTO `deathlink_2`.`user` (`username`,`password`,`email`,`account_non_expired`,`account_non_locked`,`credentials_non_expired`,`enabled`)
VALUES ("cormacx","$2a$10$fLX2c7nqe8fqmpcKH8vnEONu7WwYq04Pd8tk2pN2BfAJCcrhDSoI2","test@test.com",1,1,1,1);

INSERT INTO `deathlink_2`.`user` (`username`,`password`,`email`,`account_non_expired`,`account_non_locked`,`credentials_non_expired`,`enabled`)
VALUES ("user2","pswd2","test2@test.com",1,1,1,1);

INSERT INTO `deathlink_2`.`user` (`username`,`password`,`email`,`account_non_expired`,`account_non_locked`,`credentials_non_expired`,`enabled`)
VALUES ("user3","pswd3","test3@test.com",1,1,1,1);

INSERT INTO `deathlink_2`.`user` (`username`,`password`,`email`,`account_non_expired`,`account_non_locked`,`credentials_non_expired`,`enabled`)
VALUES ("user4","pswd4","test4@test.com",1,1,1,1);

INSERT INTO `deathlink_2`.`role` (`name`)
VALUES ("ROLE_ADMIN");

INSERT INTO `deathlink_2`.`role` (`name`)
VALUES ("ROLE_USER");

INSERT INTO `deathlink_2`.`user_has_role` (`user_id_user`, `role_id_role`)
VALUES (1,1);
INSERT INTO `deathlink_2`.`user_has_role` (`user_id_user`, `role_id_role`)
VALUES (1,2);
INSERT INTO `deathlink_2`.`user_has_role` (`user_id_user`, `role_id_role`)
VALUES (2,1);
INSERT INTO `deathlink_2`.`user_has_role` (`user_id_user`, `role_id_role`)
VALUES (3,2);
INSERT INTO `deathlink_2`.`user_has_role` (`user_id_user`, `role_id_role`)
VALUES (4,2);

INSERT INTO `deathlink_2`.`contact` ( `name`, `email`, `user_id_user`, `contact_in_case_of_death`) 
VALUES ('Seu zé do caixão', 'seu.ze.do.caixao@test.com', 1, 0);


INSERT INTO `deathlink_2`.`person` (`name`, `birthday`, `age_in_years`, `profession`, `dads_name`, `moms_name`, `is_deceased`)
VALUES ("Michael Joseph Jackson", '1958-08-29', '50', 'artist', 'Joseph Walter Jackson', 'Katherine Esther Upshaw Scruse Jackson', 1);

INSERT INTO `deathlink_2`.`person` (`name`, `birthday`, `age_in_years`, `profession`, `dads_name`, `moms_name`, `is_deceased`)
VALUES ("Elvis Aaron Presley", '1935-01-08', '42', 'artist', 'Vernon Elvis Presley', 'Gladys Love Smith Presley', 1);

INSERT INTO `deathlink_2`.`death` (`date_of_death`, `place_of_death`, `place_of_funeral`, `place_of_burial`, `date_of_burial`, `funerary`, `person_id_person`) 
VALUES ( '2009-06-25', 'Residence', 'Forest Lawn Memorial Park (Glendale)', 'Forest Lawn Memorial Park (Glendale)', '2009-09-03', 'Forest Lawn Memorial Parks & Mortuarie', 1);

INSERT INTO `deathlink_2`.`death` (`date_of_death`, `place_of_death`, `place_of_funeral`, `place_of_burial`, `date_of_burial`, `funerary`, `person_id_person`) 
VALUES ( '1977-08-16', 'Residence', 'Forest Hill Cemetary', 'Residence', '1977-08-18', '', 2);

